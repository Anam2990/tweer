<img src="https://gitlab.com/Pallavi_rai/tweer/-/raw/main/images/logo46x38.png" width="40"/>

# Share your progress

Follow us on Twitter and share some of the interesting services you come across on IBM Cloud.

<img src="https://gitlab.com/Pallavi_rai/tweer/-/raw/main/images/intro_cloud_2.png" width="600"/>

[Click here](https://twitter.com/intent/tweet?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDeveloperSkillsNetworkPY0222ENSkillsNetwork23455715-2021-01-01&text=I+just+did+static+code+analysis+as+a+part+of+the+Python+Project+for+AI+Application+development+as+a+part+of+https%3A%2F%2Fwww.coursera.org%2Flearn%2Fpython-project-for-ai-application-development) to share the above Tweet.

[Follow Rav Ahuja](https://twitter.com/ravahuja?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDeveloperSkillsNetworkPY0222ENSkillsNetwork23455715-2021-01-01) on Twitter
